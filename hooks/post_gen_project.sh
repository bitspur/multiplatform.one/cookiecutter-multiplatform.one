#!/bin/sh

set -e

if [ "$KEEP_SERVICES" = "" ]; then
    export KEEP_SERVICES="$(echo "{{cookiecutter.services}}" | tr ',' ' ')"
fi
if [ "$KEEP_PLATFORMS" = "" ]; then
    export KEEP_PLATFORMS="$(echo "{{cookiecutter.platforms}}" | tr ',' ' ')"
fi
if [ "$KEEP_PACKAGES" = "" ]; then
    export KEEP_PACKAGES="$(for p in $(ls _cloned/packages); do if [ "$(cat _cloned/packages/$p/package.json | jq -r '.private')" = "true" ]; then echo $p; fi; done)"
fi

main() {
    _merge_cloned
    _pre_cleanup
    _remove_services
    _remove_platforms
    _remove_packages
    _npm
    _vscode
    _tsconfig
    _mkpm
    _docker
    _git
    _remove_license_headers
    _done
}

_merge_cloned() {
    for f in $(cd _cloned && git ls-files); do
        mkdir -p $(dirname $f)
        echo $f
        if [ ! -f $f ]; then
            cp _cloned/$f $f
        fi
    done
    rm -rf _cloned
}

_pre_cleanup() {
    rm -rf pnpm-lock.yaml
    rm -rf platforms/storybook/.lostpixel/baseline
}

_remove_services() {
    _REMOVE_API=1
    _REMOVE_FRAPPE=1
    _REMOVE_SOLANA=1
    _REMOVE_ETHEREUM=1
    _REMOVE_SUI=1
    for b in $KEEP_SERVICES; do
        if [ "$b" = "api" ]; then
            _REMOVE_API=0
        fi
        if [ "$b" = "frappe" ]; then
            _REMOVE_FRAPPE=0
        fi
        if [ "$b" = "solana" ]; then
            _REMOVE_SOLANA=0
        fi
        if [ "$b" = "ethereum" ]; then
            _REMOVE_ETHEREUM=0
        fi
        if [ "$b" = "sui" ]; then
            _REMOVE_SUI=0
        fi
    done
    if [ "$_REMOVE_API" = "1" ]; then
        _remove_api
    fi
    if [ "$_REMOVE_FRAPPE" = "1" ]; then
        _remove_frappe
    fi
    if [ "$_REMOVE_SOLANA" = "1" ]; then
        _remove_solana
    fi
    if [ "$_REMOVE_ETHEREUM" = "1" ]; then
        _remove_ethereum
    fi
    if [ "$_REMOVE_SUI" = "1" ]; then
        _remove_sui
    fi
    if [ "$_REMOVE_API" = "1" ] && [ "$_REMOVE_FRAPPE" = "1" ]; then
        _remove_mesh
    fi
}

_remove_platforms() {
    for p in $(ls platforms); do
        _REMOVE=1
        for k in $KEEP_PLATFORMS; do
            if [ "$p" = "$k" ]; then
                _REMOVE=0
            fi
        done
        if [ "$_REMOVE" = "1" ]; then
            if [ "$p" = "expo" ]; then
                _remove_platform_expo
            fi
            if [ "$p" = "keycloak" ]; then
                _remove_platform_keycloak
            fi
            if [ "$p" = "one" ]; then
                _remove_platform_one
            fi
            if [ "$p" = "storybook" ]; then
                _remove_platform_storybook
            fi
            if [ "$p" = "storybook-expo" ]; then
                _remove_platform_storybook_expo
            fi
            if [ "$p" = "vocs" ]; then
                _remove_platform_vocs
            fi
            if [ "$p" = "vscode" ]; then
                _remove_platform_vscode
            fi
            if [ "$p" = "webext" ]; then
                _remove_platform_webext
            fi
            if [ "$p" = "electron" ]; then
                _remove_platform_electron
            fi
        fi
    done
    if [ "$(ls platforms)" = "" ]; then
        rm -rf platforms app
        find . -type f -name "package.json" -exec sh -c 'jq "del(.devDependencies.app)" "$1" > "$1.tmp" && mv "$1.tmp" "$1"' sh {} \;
    fi
}

_remove_packages() {
    for p in $(ls packages); do
        _REMOVE=1
        for k in $KEEP_PACKAGES; do
            if [ "$p" = "$k" ]; then
                _REMOVE=0
            fi
        done
        if [ "$_REMOVE" = "1" ]; then
            rm -rf "packages/$p"
        fi
    done
    if [ "$(ls packages)" = "" ]; then
        rm -rf packages
    fi
}

_npm() {
    cat package.json | jq '.name = "{{cookiecutter.name}}"' | sponge package.json
    cat package.json | jq '.scripts.build = "multiplatform.one build packages"' | sponge package.json
    find . -type f -name "package.json" -exec sh -c 'sed "s|\"\([^\"]*\)\": \"workspace:\([^\"*]*\)\"|\"\1\": \"\2\"|g" "$1" > "$1.tmp" && mv "$1.tmp" "$1"' sh {} \;
    pnpm install
    jq 'del(.packageManager)' package.json | sponge package.json
}

_vscode() {
    jq '.compounds[] |= if .configurations == [] then empty else . end' .vscode/launch.json | sponge .vscode/launch.json
    cat .vscode/extensions.json | jq '.recommendations |= map(select(. != "psioniq.psi-header"))' | sponge .vscode/extensions.json
    cat .vscode/settings.json | jq 'del(.["psi-header.changes-tracking"])' | sponge .vscode/settings.json
    cat .vscode/settings.json | jq 'del(.["psi-header.config"])' | sponge .vscode/settings.json
    cat .vscode/settings.json | jq 'del(.["psi-header.lang-config"])' | sponge .vscode/settings.json
    cat .vscode/settings.json | jq 'del(.["psi-header.license-reference"])' | sponge .vscode/settings.json
    cat .vscode/settings.json | jq 'del(.["psi-header.license-text"])' | sponge .vscode/settings.json
    cat .vscode/settings.json | jq 'del(.["psi-header.templates"])' | sponge .vscode/settings.json
    cat .vscode/settings.json | jq 'del(.["psi-header.variables"])' | sponge .vscode/settings.json
}

_mkpm() {
    rm -rf .mkpm
    ./mkpm - reset
    sed -i ':a;N;$!ba;s/\n\{3,\}/\n\n/g' Mkpmfile
}

_docker() {
    rm -rf docker/devcontainer
    rm -rf docker/dns
}

_tsconfig() {
    for f in $(find . -type f -name "tsconfig*.json" -not -path "*/node_modules/*"); do
        for p in $(cat $f | jq -r '(.references // [])[].path'); do
            if [ ! -f "$p" ]; then
                cat "$f" | jq "del(.references[] | select(.path == \"$p\"))" | sponge "$f"
            fi
        done
    done
}

_done() {
    cp .env.default .env
    for f in $(git ls-files "*.yaml" "*.yml"); do
        format_yaml "$f"
    done
    ./mkpm check/lint || true
    rm -rf .git
    git init
    git add -A
    git commit -m "Initial commit"
}

_remove_api() {
    jq "del(.configurations[] | select(.name == \"api dev\"))" .vscode/launch.json | sponge .vscode/launch.json
    jq "del(.compounds[].configurations[] | select(. == \"api dev\"))" .vscode/launch.json | sponge .vscode/launch.json
    cat Mkpmfile | sed "/[\t, ]api\//d" | sed "/[\t, ]api)/d" | sed "/^api\//d" | sponge Mkpmfile
    if [ -f "api/.meshrc.yaml" ]; then
        cat api/.meshrc.yaml | yaml2json | jq 'del(.sources[] | select(.name == "Api"))' | json2yaml > api/.meshrc.yaml
    fi
    jq 'del(.prisma) |
        .dependencies |= with_entries(select(
            .key == "@escape.tech/graphql-armor-block-field-suggestions" or
            .key == "@escape.tech/graphql-armor-max-depth" or
            .key == "@escape.tech/graphql-armor-max-tokens" or
            .key == "@graphql-mesh/cli" or
            .key == "@graphql-mesh/graphql" or
            .key == "@graphql-mesh/plugin-operation-field-permissions" or
            .key == "@graphql-mesh/plugin-rate-limit" or
            .key == "@graphql-mesh/plugin-response-cache" or
            .key == "@graphql-mesh/transform-filter-schema" or
            .key == "@graphql-mesh/transform-prefix"
        )) |
        .devDependencies |= with_entries(select(
            .key == "@multiplatform.one/cli"
        )) |
        del(.scripts.build) |
        del(.scripts.dev) |
        del(.scripts.generate) |
        del(.scripts.seed)' api/package.json | sponge api/package.json
    rm -rf api/prisma
    sed -i '/^# api$/d' .env.default
    sed -i '/^API_/d' .env.default
    sed -i '/^MESH_API=/d' .env.default
    sed -i '/^# prisma$/d' .env.default
    sed -i '/^PRISMA_/d' .env.default
    sed -i '/^# sqlite$/d' .env.default
    sed -i '/^SQLITE_/d' .env.default
}

_remove_frappe() {
    rm -rf "frappe"
    jq "del(.configurations[] | select(.name == \"frappe dev\"))" .vscode/launch.json | sponge .vscode/launch.json
    jq "del(.compounds[].configurations[] | select(. == \"frappe dev\"))" .vscode/launch.json | sponge .vscode/launch.json
    cat Mkpmfile | sed "/[\t, ]frappe\//d" | sed "/[\t, ]frappe)/d" | sed "/^frappe\//d" | sponge Mkpmfile
    if [ -f "api/.meshrc.yaml" ]; then
        cat api/.meshrc.yaml | yaml2json | jq 'del(.sources[] | select(.name == "Frappe"))' | json2yaml > api/.meshrc.yaml.tmp
        mv api/.meshrc.yaml.tmp api/.meshrc.yaml
    fi
    sed -i '/frappe/d' .devcontainer/postattach.sh
    sed -i '/^# frappe$/d' .env.default
    sed -i '/^FRAPPE_/d' .env.default
    sed -i '/^MESH_FRAPPE=/d' .env.default
}

_remove_solana() {
    rm -rf "solana"
    jq "del(.configurations[] | select(.name == \"solana dev\"))" .vscode/launch.json | sponge .vscode/launch.json
    jq "del(.compounds[].configurations[] | select(. == \"solana dev\"))" .vscode/launch.json | sponge .vscode/launch.json
    cat Mkpmfile | sed "/[\t, ]solana\//d" | sed "/[\t, ]solana)/d" | sed "/^solana\//d" | sponge Mkpmfile
}

_remove_ethereum() {
    rm -rf "ethereum"
    jq "del(.configurations[] | select(.name == \"ethereum dev\"))" .vscode/launch.json | sponge .vscode/launch.json
    jq "del(.compounds[].configurations[] | select(. == \"ethereum dev\"))" .vscode/launch.json | sponge .vscode/launch.json
    cat Mkpmfile | sed "/[\t, ]ethereum\//d" | sed "/[\t, ]ethereum)/d" | sed "/^ethereum\//d" | sponge Mkpmfile
}

_remove_sui() {
    rm -rf "sui"
    jq "del(.configurations[] | select(.name == \"sui dev\"))" .vscode/launch.json | sponge .vscode/launch.json
    jq "del(.compounds[].configurations[] | select(. == \"sui dev\"))" .vscode/launch.json | sponge .vscode/launch.json
    cat Mkpmfile | sed "/[\t, ]sui\//d" | sed "/[\t, ]sui)/d" | sed "/^sui\//d" | sponge Mkpmfile
}

_remove_mesh() {
    jq "del(.configurations[] | select(.name == \"api mesh\"))" .vscode/launch.json | sponge .vscode/launch.json
    jq "del(.compounds[].configurations[] | select(. == \"api mesh\"))" .vscode/launch.json | sponge .vscode/launch.json
    rm -rf api
    sed -i '/^# mesh$/d' .env.default
    sed -i '/^MESH_/d' .env.default
    sed -i '/^# postgres$/d' .env.default
    sed -i '/^POSTGRES_/d' .env.default
}

_remove_platform_keycloak() {
    jq "del(.configurations[] | select(.name == \"keycloak dev\"))" .vscode/launch.json | sponge .vscode/launch.json
    jq "del(.compounds[].configurations[] | select(. == \"keycloak dev\"))" .vscode/launch.json | sponge .vscode/launch.json
    rm -rf "platforms/keycloak"
    cat Mkpmfile | sed "/[\t, ]keycloak\//d" | sed "/[\t, ]keycloak)/d" | sed "/^keycloak\//d" | sponge Mkpmfile
}

_remove_platform_one() {
    jq "del(.configurations[] | select(.name == \"one dev\"))" .vscode/launch.json | sponge .vscode/launch.json
    jq "del(.compounds[].configurations[] | select(. == \"one dev\"))" .vscode/launch.json | sponge .vscode/launch.json
    rm -rf "platforms/one"
    cat Mkpmfile | sed "/[\t, ]one\//d" | sed "/[\t, ]one)/d" | sed "/^one\//d" | sponge Mkpmfile
    sed -i '/^# one$/d' .env.default
    sed -i '/^ONE_/d' .env.default
}

_remove_platform_storybook() {
    jq "del(.configurations[] | select(.name == \"storybook dev\"))" .vscode/launch.json | sponge .vscode/launch.json
    jq "del(.compounds[].configurations[] | select(. == \"storybook dev\"))" .vscode/launch.json | sponge .vscode/launch.json
    cat Mkpmfile | sed "/[\t, ]storybook\//d" | sed "/[\t, ]storybook)/d" | sed "/^storybook\//d" | sponge Mkpmfile
    rm -rf "platforms/storybook"
    sed -i '/^# storybook$/d' .env.default
    sed -i '/^STORYBOOK_/d' .env.default
    sed -i '/^VR_DIFFING_ENGINE=/d' .env.default
}

_remove_platform_storybook_expo() {
    jq "del(.configurations[] | select(.name == \"storybook-expo dev\"))" .vscode/launch.json | sponge .vscode/launch.json
    jq "del(.compounds[].configurations[] | select(. == \"storybook-expo dev\"))" .vscode/launch.json | sponge .vscode/launch.json
    cat Mkpmfile | sed "/[\t, ]storybook-expo\//d" | sed "/[\t, ]storybook-expo)/d" | sed "/^storybook-expo\//d" | sponge Mkpmfile
    rm -rf "platforms/storybook-expo"
}

_remove_platform_vocs() {
    jq "del(.configurations[] | select(.name == \"vocs dev\"))" .vscode/launch.json | sponge .vscode/launch.json
    jq "del(.compounds[].configurations[] | select(. == \"vocs dev\"))" .vscode/launch.json | sponge .vscode/launch.json
    rm -rf "platforms/vocs"
    sed -i '/^# vocs$/d' .env.default
    sed -i '/^VOCS_/d' .env.default
}

_remove_platform_vscode() {
    jq "del(.configurations[] | select(.name == \"vscode dev\"))" .vscode/launch.json | sponge .vscode/launch.json
    jq "del(.compounds[].configurations[] | select(. == \"vscode dev\"))" .vscode/launch.json | sponge .vscode/launch.json
    rm -rf "platforms/vscode"
    sed -i '/^# vscode$/d' .env.default
    sed -i '/^VSCODE_/d' .env.default
}

_remove_platform_webext() {
    jq "del(.configurations[] | select(.name == \"webext dev\"))" .vscode/launch.json | sponge .vscode/launch.json
    jq "del(.compounds[].configurations[] | select(. == \"webext dev\"))" .vscode/launch.json | sponge .vscode/launch.json
    rm -rf "platforms/webext"
    sed -i '/^# webext$/d' .env.default
    sed -i '/^WEBEXT_/d' .env.default
}

_remove_platform_electron() {
    jq "del(.configurations[] | select(.name == \"electron dev\"))" .vscode/launch.json | sponge .vscode/launch.json
    jq "del(.compounds[].configurations[] | select(. == \"electron dev\"))" .vscode/launch.json | sponge .vscode/launch.json
    rm -rf "platforms/electron"
    sed -i '/^# electron$/d' .env.default
    sed -i '/^ELECTRON_/d' .env.default
}

_remove_hash_license_header() {
    awk '
BEGIN { in_header = 0; lines_seen = 0; }
/^# File:/ || /^# Project:/ || /^# File Created:/ || /^# Author:/ {
    if (lines_seen <= 1) {
        in_header = 1;
        next;
    }
}
in_header {
    if ($0 !~ /^#/) {
        in_header = 0;
        if ($0 ~ /^$/) next;
        print;
    }
    next;
}
{
    if (!in_header) {
        print;
        lines_seen++;
    }
}
' "$1" | sponge "$1"
    awk 'NF {p=1} p' "$1" | sponge "$1"
}

_remove_block_license_header() {
    awk '
BEGIN { in_block = 0; print_block = 1; }
/^\/\*/ {
    in_block = 1;
    buffer = $0 "\n";
    next;
}
in_block {
    buffer = buffer $0 "\n";
    if ($0 ~ /\*  File:|\*  Project:|\*  File Created:|\*  Author:|\* File:|\* Project:|\* File Created:|\* Author:/) {
        print_block = 0;
    }
    if ($0 ~ /\*\//) {
        if (print_block) {
            printf "%s", buffer;
        }
        in_block = 0;
        print_block = 1;
        buffer = "";
        next;
    }
}
{
    if (!in_block) print;
}
' "$1" | sponge "$1"
    awk 'NF {p=1} p' "$1" | sponge "$1"
}

_git() {
    git init
    git add -A
    git commit -m "tmp commit"
}

_remove_license_headers() {
    for f in $(git ls-files); do
        case "$f" in
            *.ts|*.tsx|*.js|*.jsx|*.mts|*.mtsx|*.mjs|*.mjsx|*.cts|*.ctsx|*.cjs|*.cjsx)
                _remove_block_license_header "$f"
                ;;
            *.sh|*.mk|*.yaml|Mkpmfile|Makefile|*.py)
                _remove_hash_license_header "$f"
                ;;
        esac
    done
}

sponge() {
    _TEMP=$(mktemp)
    cat > "$_TEMP"
    mv "$_TEMP" "$1"
}

yaml2json() {
    perl -MYAML::XS=Load -MJSON::XS=encode_json -e '
        local $/;
        my $yaml = <STDIN>;
        my $data = Load($yaml);
        print encode_json($data);
    '
}

json2yaml() {
    perl -MJSON::XS=decode_json -MYAML::XS=Dump -e '
        local $/;
        my $json = <STDIN>;
        my $data = decode_json($json);
        $YAML::XS::UseHeader = 0;
        print Dump($data);
    '
}

format_yaml() {
    node -e '
        const yaml = require("yaml");
        const fs = require("fs");
        for (const file of process.argv.slice(1)) {
            const data = fs.readFileSync(file, "utf8");
            const doc = yaml.parse(data);
            const formatted = yaml.stringify(doc, {
                indent: 2,
                indentSeqItems: true
            });
            fs.writeFileSync(file, formatted);
        }
    ' "$@"
}

main "$@"
