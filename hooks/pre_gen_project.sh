#!/bin/sh

BASE_PATH="${BASE_PATH:-https://gitlab.com/bitspur/multiplatform.one/multiplatform.one.git}"
BRANCH="${BRANCH:-main}"

export GIT_CLONE_PROTECTION_ACTIVE=false
if echo "$BASE_PATH" | grep -qE '^https?://|^ssh://|git@'; then
    git clone --depth=1 --branch "$BRANCH" "$BASE_PATH" _cloned
else
    mkdir _cloned
    cd _cloned
    for f in $(cd $BASE_PATH && git ls-files); do
        mkdir -p $(dirname $f)
        cp $BASE_PATH/$f $f
    done
    git init
    git add -A
    cd ..
fi
